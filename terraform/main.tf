terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
    }
  }
}


provider "google" {
  credentials = file("softwareavanzado-342900-2eced1550abf.json") 
  project     = "softwareavanzado-342900"
  region      = "us-central1"
  zone        = "us-central1-c"
}



resource "google_compute_firewall" "regla-firewall" {
  name    = "tp9-201513626"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["8081", "3000"]
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["regla-firewall"]
}



resource "google_compute_instance" "maquinavirtual" {

  name         = "tp9-201513626"
  machine_type = "e2-medium"
  tags         = ["regla-firewall"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  network_interface {
    network = "default"
    access_config {
      nat_ip       = "35.209.251.156"
      network_tier = "STANDARD"
    }
  }

  metadata_startup_script = file("./script")

  depends_on = [google_compute_firewall.regla-firewall]
}



terraform {
  backend "http" {}
}
