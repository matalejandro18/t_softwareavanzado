import requests
import unittest

from requests.api import request
import json

URL = "http://localhost:3000"

endpoints = {
    "InsertarDatos" : "/DatosEstudiantes",
    "MostarDatos" : "/MostarDatos",   
}


#Cosas importantes
#todos los metodos deben iniciar con test
# response.json() <- obtiene el json de respuesta
# response.status_code  <- retorna el estado
# response.tex()


class UsersTest(unittest.TestCase):
    #Insertar dato, de ser correcto retorna un estado 200
    def test_create_Estudiante(self):
        print("Prueba Insertar Estudiante")
        info = {
            "carnet" : "201513626",
            "nombre" : "ALberto"
        }
        response = requests.post(URL+endpoints["InsertarDatos"],data=json.dumps(info), headers={'Content-Type': 'application/json'})
        statuscode = response.status_code
        self.assertEqual(statuscode,200)

    #Mostar Datos
    def test_get_Estudiante(self):
        print("Prueba de Mostrar Estudiantes")
        response = requests.get(URL+endpoints["MostarDatos"])
        self.assertEqual(response.status_code,200)



