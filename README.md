# SoftwareAvanzado
# Tarea de practica 9

## Integrante:
- Manuel Alejandro De Mata Mayen - 201513626 


### Procedimiento de tareas

### Creacion de Dockerfile para las pruebas Unitarias

![subred](./Imagenes/dockerpruebas.png "get")

- Se creo el dockerfile del proyecto Ubuntu para tener el contenedor de ubuntu

### GITLAB-CI.yml

![subred](./Imagenes/StagePruebas.png "get")

- se colocal el nombre de la imagen de docker para la instalacion 
- el nombre del servicio a utilizar
- agregamos los stage y se colocan en el orden que deseamos que se ejecuten.
- podemos observar  el primer stagemen que es sobre las ejecucion de pruebas en el ci/cd
- agregamos el nombre del stage antes colocado.
- agregamos un script para para que ejecute el programa.
- creamos los artefactos.
- que realice todo al hacer push ala rama feature/tp_9


![subred](./Imagenes/stageProbandoPruebas.png "get")

- el nombre del stage se coloco pruebas.
- de busca la imagen del docker compose
- se genera un script antes de la ejecucion, en donde se realiza un docker-compose up para arrancar los contenedores-
- el script carga el archivo de pruebas y luego las ejecuta el docker compose y al terminar todo lo baja.
- se crea el artefacto de pruebas-
- todo esto se realiza en la rama del reporsitorio de feature/tp_9
- y un apartado de needs, que este stage necesita el de pruebas-cicd para poder realizar la ejecucion


![subred](./Imagenes/stageConstruccionImagen.png "get")

- Nombre del stage Build.
- script en donde se subiran nuestras imagenes al docker hub, como se puede observar se subio la imagen de Backend y Frontend
- se genera los artefactos de frontend y backend
- se realizara en la rama de develop



![subred](./Imagenes/stagePublicacion.png "get")

- Nombre de stage publicacion.
- se inicia con el login de docker hub colocando el nombre y contraseña.
- se subo las imagenes al docker hub.
- todo se contruye al hacer merge a develop

### TERRAFORM

![subred](./Imagenes/Terraform.png "get")

- se agrega las credenciales de nuestra cuenta de google cloud.
- de trabaja en el proyecto de softwareavanzado-342900
- en la region de us-central1
- se crea la regla de firewall, con el nombre de tip9-201513626
- se expondra en el puerto 8081
- se crea la maquina virtual de con el nombre de tp9-201513626
- la maquina virtual es de tipo e2-medium.
- se le agrega una ip reservada que es la 35.209.251.156.

![subred](./Imagenes/script.png "get")

- Aqui se ejecutara todos los comandos que deseemos que corra la maquina virtual.
- actualizacion del sistema.
- instalacion del docker.io
- instalacion de git
- instalacion de docker-compose
- creacion del volumen my-db
- clonacion de nuestro repositorio.
- entramos ala carpeta, t_softwareavanzado
- y ejecutamos el docker-compose