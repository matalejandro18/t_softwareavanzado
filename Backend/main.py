from flask import Flask,request,jsonify ,redirect, url_for, flash,make_response
import requests
import json
from flask_cors import CORS
import mysql.connector

app = Flask(__name__)
app.config['MYSQL_DATABASE_HOST'] = '192.168.69.5'
app.config['MYSQL_USER'] = 'root' #userdb
app.config['MYSQL_PASSWORD'] = 'manuel5470' #userpwd
app.config['MYSQL_DB'] = 'Tarea7'

db_creds = {
    "user"      : "root",
    "password"  : "manuel5470",
    "host"      : "192.168.69.5",
    "port"      : "23306",
    "database"  : "Tarea7"
}

datos = """INSERT INTO Estudiantes(carnet,nombre) VALUES (%s, %s)"""


#mysql = MySQL(app)

CORS(app)

# En esta linea introduzco las credenciales de mi base de datos, en este caso la base para repartidores
#mysql = mysql.connector.connect( host='localhost', user= 'root', passwd='manuel5470', db='Tarea7' )


@app.route('/')
def Index():
    return 'TAREA-201513626'

@app.route('/DatosEstudiantes', methods=['POST'])
def DatosEstudiantes():

    if request.method == 'POST':
        content = request.get_json()

        carnet = content['carnet']
        nombre = content['nombre']
        try:
            db = mysql.connector.connect(user="root", 
            password="manuel5470", host="192.168.69.5", 
            database="Tarea7", auth_plugin='mysql_native_password')
            cur = db.cursor()
            print(carnet)
            print(nombre)
            cur.execute(datos, (carnet, nombre))
            db.commit()
            cur.close()
            db.close()

            response = make_response(jsonify({"result": 'insertado'}),200)
            response.headers["Content-Type"] = "application/json"
            return response
        except Exception as e:
            return  {'Error': 'Error - ' + str(e)}

#pruebas
@app.route('/MostarDatos', methods=['GET'])
def MostarDatos():

    try:
        db = mysql.connector.connect(user="root", 
        password="manuel5470", host="192.168.69.5", 
        database="Tarea7", auth_plugin='mysql_native_password')
        cur = db.cursor()
        cur.execute('select * from Estudiantes')
        data = cur.fetchall()
        cur.close()
        db.close()
        pedidos = []
        for fila in data:
            pedido = {'carnet': fila[0], 'nombre': fila[1]}
            pedidos.append(pedido)
            
        print(pedidos)
        return make_response(jsonify(pedidos))
    except Exception as e:
 

        return  make_response(jsonify({'Error': 'Error - ' +str(e) }))


def main():
    db = mysql.connector.connect(user="root",password="manuel5470", host="192.168.69.5")
    cur = db.cursor()
    cur.execute("CREATE database IF NOT EXISTS Tarea7; CREATE TABLE IF NOT EXISTS Tarea7.Estudiantes (carnet int, nombre VARCHAR(50))")
    cur.close()
    app.run(host='0.0.0.0',port=3000, debug=True) 

if __name__ == '__main__':
    main()
