import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Estudiantes} from '../model/Estudiante';
//prueba
@Injectable({
  providedIn: 'root'
})
export class EstudianteService {

  API_URI = 'http://35.209.251.156:3000'

  constructor(private http: HttpClient) { }

  getEstudiantes() {
    return this.http.get(`${this.API_URI}/MostarDatos`);
  }

  AddEstudiantes(estudiante: Estudiantes) {
    return this.http.post(`${this.API_URI}/DatosEstudiantes`, estudiante);
  }
}

//pruebas