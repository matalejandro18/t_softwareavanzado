import { Component, OnInit, HostBinding } from '@angular/core';

import {EstudianteService} from '../../services/estudiante.service'

@Component({
  selector: 'app-estudiante-list',
  templateUrl: './estudiante-list.component.html',
  styleUrls: ['./estudiante-list.component.css']
})
export class EstudianteListComponent implements OnInit {

  @HostBinding('class') classes = 'row';

  estudiantes: any = [];

  constructor(private estudianteService: EstudianteService) { }

  ngOnInit(): void {
    this.estudianteService.getEstudiantes().subscribe(
      res => {
        console.log(res);
        this.estudiantes = res;
        
      },
      err => console.error(err)

    );
  }

}
