import { Component, OnInit, HostBinding } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {Estudiantes} from '../../model/Estudiante';

import {EstudianteService} from '../../services/estudiante.service';

@Component({
  selector: 'app-estudiante-form',
  templateUrl: './estudiante-form.component.html',
  styleUrls: ['./estudiante-form.component.css']
})
export class EstudianteFormComponent implements OnInit {

  @HostBinding('class') clases = 'row';

  estudiante: Estudiantes = {
    carnet: 0,
    nombre: ''
  }

  constructor(private estudianteService: EstudianteService,private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
  }

  guardarEstudinate() {
    this.estudianteService.AddEstudiantes(this.estudiante).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/getEstudiante']);
      }
    )
  }

}
