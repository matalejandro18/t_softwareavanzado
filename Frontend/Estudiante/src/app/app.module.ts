import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavegationComponent } from './component/navegation/navegation.component';
import { EstudianteFormComponent } from './component/estudiante-form/estudiante-form.component';
import { EstudianteListComponent } from './component/estudiante-list/estudiante-list.component';

import { EstudianteService} from './services/estudiante.service'
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    NavegationComponent,
    EstudianteFormComponent,
    EstudianteListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    EstudianteService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
