import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {EstudianteFormComponent} from './component/estudiante-form/estudiante-form.component';
import {EstudianteListComponent} from './component/estudiante-list/estudiante-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/getEstudiante',
    pathMatch: 'full'
  },
  {
    path: 'getEstudiante',
    component: EstudianteListComponent
  },
  {
    path: 'AddEstudiante',
    component: EstudianteFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
